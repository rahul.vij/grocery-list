import React from "react";
import { useState, useEffect } from "react";
import Model from "../model/MainModel";
import util from "./util";
import MainView from "../view/MainView";

function MainController() {
    const [myList, setMyList] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(function () {
        setMyList(() => {
            setLoading(false);
            return Model.getListStorage();
        });
    }, []);

    useEffect(
        function () {
            Model.setListStorage(myList);
        },
        [myList]
    );

    function addGroceryListItem(item) {
        // Adds a Grocery List Item to the Grocery List
        let list = Model.list;
        const foundItem = util.checkSameName(item);
        if (foundItem) {
            item.itemQuantity += foundItem.itemQuantity;
            item.itemId = foundItem.itemId;
            updateGroceryListItem(item);
            return "Quantity Incremented Successfully !";
        } else {
            item.itemId = Date.now().toString();
            setMyList((list) => {
                list = [...list, item];
                return list;
            });
            return "Item Added Successfully !";
        }
    }
    function updateGroceryListItem(item) {
        // Updates a Grocery List Item present in the Grocery List
        let list = Model.list;
        const foundItem = util.checkSameId(item);
        if (foundItem) {
            for (let key in foundItem) {
                foundItem[key] = item[key];
            }
            const newFoundItem = util.checkSameNameDiffId(foundItem);
            if (newFoundItem) {
                return "Cannot Have Two Items With Same Name !";
            }
            const indexOfFoundItem = list.indexOf(foundItem);
            setMyList((prevList) => {
                prevList[indexOfFoundItem] = item;
                return [...prevList];
            });
        } else {
            return "The Item Does Not Exist In The List !";
        }
        return "Successfully Updated !";
    }
    function removeGroceryListItem(item) {
        // Removes a Grocery List Item present in the Grocery List
        setMyList((list) => {
            list = list.filter((listItem) => {
                return listItem.itemName !== item.itemName;
            });
            return list;
        });
        return "Item Successfully Removed !";
    }
    if (loading) {
        return <div>LOADING...</div>;
    } else {
        return (
            <>
                <MainView
                    util={{
                        addGroceryListItem,
                        removeGroceryListItem,
                        updateGroceryListItem,
                    }}
                    list={myList}
                />
            </>
        );
    }
}

export default MainController;
