import Model from "../model/MainModel";

const util = {
    
    checkSameName: function (item) {
        // Checks if there is an grocery list item with the same name already present
        let list = Model.list;
        const foundItem = list.find((listItem) => {
            return listItem.itemName === item.itemName;
        });
        return foundItem;
    },
    checkSameId: function (item) {
        // Checks if there is an grocery list item with the same id already present
        let list = Model.list;
        const foundItem = list.find((listItem) => {
            return listItem.itemId === item.itemId;
        });
        return foundItem;
    },
    checkSameNameDiffId: function (item) {
        // Checks if there is an grocery list item with the same name but different id already present
        let list = Model.list;
        console.log(list, item);
        const foundItem = list.find((listItem) => {
            return (
                listItem.itemName === item.itemName &&
                listItem.itemId !== item.itemId
            );
        });
        return foundItem;
    }
}

export default util;
