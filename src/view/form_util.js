const util = {
    accessDOM: function () {
        this.groceryList = document.querySelector(".grocery-list");
        this.form = document.querySelector(".form-holder>form");
        this.formTitle = document.querySelector(".form-section-title >p");
        this.formSubmitBtn = document.querySelector(".form-holder #inputItem");
        this.cancelEditBtn = document.querySelector(".form-holder .cancelBtn");
        this.currentForm = "add";
    },
    attachELToForm: function ({ addGroceryListItem, updateGroceryListItem }) {
        // Attaches event listeners to the save/add/cancel buttons on the form
        this.accessDOM();
        this.form.addEventListener("submit", (event) => {
            event.preventDefault();
            const data = new FormData(event.target);
            const item = Object.fromEntries(data.entries());
            if (item.itemName === "" || item.itemQuantity === "") {
                alert("Please Fill All The Fields");
                // location.reload();
                return;
            }
            item.itemName = item.itemName.toUpperCase();
            item.itemQuantity = parseInt(item.itemQuantity, 10);
            this.form.querySelector("#inputItemName").value = "";
            this.form.querySelector("#inputItemQuantity").value = "";
            if (this.currentForm === "add") {
                let message = addGroceryListItem(item);
                alert(message);
            } else {
                item.itemId = this.form.getAttribute("itemId");
                let message = updateGroceryListItem(item);
                alert(message);
            }
        });
        this.cancelEditBtn.addEventListener("click", () => {
        });
    },
};
export default util;
