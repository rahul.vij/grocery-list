import React from "react";
import Header from "./Header";
import Form from "./Form";
import Grocery from "./Grocery";
import { useState, useEffect } from "react";
import mainview_util from "./mainview_util";

function MainView({ util, list }) {
    const [currForm, setCurrForm] = useState("add");
    useEffect(function () {
        mainview_util.accessDOM();
    }, []);
    useEffect(
        function () {
            if (currForm === "add") mainview_util.toggleForm("add");
            else mainview_util.toggleForm("edit");
        },
        [currForm]
    );

    return (
        <>
            <Header />
            <div className="main">
                <Form
                    util={util}
                    setCurrForm={setCurrForm}
                />
                <Grocery util={util} list={list} setCurrForm={setCurrForm} />
            </div>
        </>
    );
}

export default MainView;
