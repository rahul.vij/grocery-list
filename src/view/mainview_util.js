const util = {
    accessDOM: function () {
        this.form = document.querySelector(".form-holder>form");
        this.formTitle = document.querySelector(".form-section-title >p");
        this.formSubmitBtn = document.querySelector(".form-holder #inputItem");
        this.cancelEditBtn = document.querySelector(".form-holder .cancelBtn");
    },
    toggleForm: function (data) {
        // Toggles between the edit and add item states
        this.form.querySelector("#inputItemName").value = "";
        this.form.querySelector("#inputItemQuantity").value = "";
        if (data === "add") {
            this.formTitle.textContent = "Add Item";
            this.form.setAttribute("itemId", "");
            this.cancelEditBtn.classList.add("hidden");
            this.formSubmitBtn.value = "Add";
        } else {
            this.formTitle.textContent = "Edit Item";
            this.cancelEditBtn.classList.remove("hidden");
            this.formSubmitBtn.value = "Save";
        }
    }
};
export default util;
