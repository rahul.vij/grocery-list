const util = {
    accessDOM: function () {
        this.form = document.querySelector(".form-holder>form");
    },
    attachELToGroceryListItem: function (
        groceryListItem,
        { removeGroceryListItem }
    ) {
        // Attaches event listeners to the edit and delete buttons of each grocery list item
        this.accessDOM();
        const itemName = groceryListItem.getAttribute("itemName");
        const itemQuantity = parseInt(
            groceryListItem.getAttribute("itemQuantity"),
            10
        );
        const itemId = groceryListItem.getAttribute("itemId");
        const editBtn = groceryListItem.querySelector(".edit");
        const deleteBtn = groceryListItem.querySelector(".delete");

        editBtn.addEventListener("click", () => {
                this.form.querySelector("#inputItemName").value = itemName;
                this.form.querySelector("#inputItemQuantity").value =
                    itemQuantity;
                this.form.setAttribute("itemId", itemId);
        });

        deleteBtn.addEventListener("click", () => {
            let message = removeGroceryListItem({
                itemName,
                itemQuantity,
                itemId,
            });
            alert(message);
        });
    },
};

export default util;
